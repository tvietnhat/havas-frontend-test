'use strict';

// sass compile
var gulp = require('gulp');
var sass = require('gulp-sass');
var prettify = require('gulp-prettify');
var minifyCss = require("gulp-minify-css");
var rename = require("gulp-rename");
var uglify = require("gulp-uglify");
var rtlcss = require("gulp-rtlcss");
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var mainBowerFiles = require('gulp-main-bower-files');
var filter = require('gulp-filter');
var order = require('gulp-order');
var historyApiFallback = require('connect-history-api-fallback');
var browserSync = require('browser-sync').create();
var ngAnnotate = require('gulp-ng-annotate');
var rev = require('gulp-rev-append');
var portfinder = require('portfinder');

var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'compress'
};
var autoprefixerOptions = {
    browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};


//*** Bower tasks
gulp.task('bower:fonts', function () {

    return gulp.src([
        './bower_components/bootstrap/dist/fonts/**/*',
        './bower_components/font-awesome/fonts/**/*',
    ])
        .pipe(gulp.dest('./public/assets/fonts'));
});
gulp.task('bower:js', function () {

    var filterJS = filter('**/*.js');

    return gulp.src('./bower.json')
        .pipe(mainBowerFiles({
            overrides: {
                bootstrap: {
                    main: [
                        './dist/js/bootstrap.js'
                    ]
                },
				'ng-clipboard': {
                    main: [
                        './dist/ng-clipboard.js'
                    ]
				},
				'angular-jquery': {
                    main: [
                        './dist/angular-jquery.js'
                    ]
				}
            }
        }))
        .pipe(filterJS)
        .pipe(sourcemaps.init())
        .pipe(concat('bower.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./maps'))
        //.pipe(filterJS.restore)
        .pipe(gulp.dest('./public/assets/js'));
});
gulp.task('bower:css', function () {

    var filterCSS = filter('**/*.css');

    gulp.src('./bower.json')
        .pipe(mainBowerFiles({
            overrides: {
                bootstrap: {
                    main: [
                        './dist/css/bootstrap.css'
                    ]
                },
                'angular-resizable': {
                    main: [
                        './src/angular-resizable.css'
                    ]
                },
                'font-awesome': {
                    main: [
                        './css/font-awesome.css'
                    ]
                }
            }
        }))
        .pipe(filterCSS)
        .pipe(order([
            'bootstrap/dist/css/bootstrap.css',
            '*'
        ]))
        .pipe(sourcemaps.init())
        .pipe(concat('bower.css'))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./public/assets/css'));

});


//*** Angular app tasks
gulp.task('app:css', function () {

    return gulp.src(['./sass/app/**/*.scss', './app/**/*.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass(sassOptions))
        .pipe(concat('app.css'))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./public/assets/css'));

});
gulp.task('app:css:prefix', ['app:css'], function () {

    return gulp.src('./public/assets/css/app.css')
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(gulp.dest('./public/assets/css'));

});
gulp.task('app:js:concat', function () {

    gulp.src([
        './app/**/*.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(ngAnnotate({
            // true helps add where @ngInject is not used. It infers.
            // Doesn't work with resolve, so we must be explicit there
            // @see https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md#style-y101
            add: true
        }))
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./public/assets/js'))
    ;

    // Cache Busting
    gulp.src('./app/index.html')
        .pipe(gulp.dest('./public'))
		.pipe(rev())
    	.pipe(gulp.dest('./public'))
    ;

});


function bsWatch() {
    gulp.watch(['./sass/app/**/*.scss', './app/**/*.scss'], ['app:css', 'app:css:prefix']);
    gulp.watch(['./env.js', './app/**/*.js'], ['app:js:concat']);
    gulp.watch(['./public/**/*', '.custom.js']).on('change', browserSync.reload);
}

gulp.task('app:watch', function () {
    portfinder.getPort(function (err, port) {
		if (err) {
			console.log('Error finding an available port', err);
		} else {
		    browserSync.init({
		        server: {
		            baseDir: './public',
		            middleware: [historyApiFallback()]
		        },
				snippetOptions: {
			        ignorePaths: "admin/tools"
			    }
		    });

			bsWatch();
		}
    });
});


gulp.task('default', ['bower:fonts', 'bower:js', 'bower:css', 'app:css', 'app:css:prefix', 'app:js:concat']);


//*** HTML formatter task
gulp.task('prettify', function () {

    gulp.src('./**/*.html').pipe(prettify({
        indent_size: 4,
        indent_inner_html: true,
        unformatted: ['pre', 'code']
    })).pipe(gulp.dest('./'));
});

