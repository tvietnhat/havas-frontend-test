## Installation

```bash
npm install
bower install
gulp
```

## Run the app and watch for your changes
```bash
gulp app:watch
```
