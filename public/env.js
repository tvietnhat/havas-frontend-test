(function() {
    'use strict';

    angular.module('env',[])

        // Installation specific settings
        .constant('CONFIG',{
            ENV: 'dev' // dev OR prod
        })
    ;

})();

// Installation specific settings
var SETTINGS = {
    AUTH: {
        TYPE: 'none',
        LOGIN_URL: '',
        FORGOT_PASSWORD: false
    },
};