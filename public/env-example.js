(function() {
    'use strict';

    angular.module('env',[])

        // Installation specific settings
        .constant('CONFIG',{
            ENV: 'dev' // dev OR prod
        })
    ;

})();

// Installation specific settings
var SETTINGS = {
    AUTH: {
        TYPE: 'none',
        LOGIN_URL: '',
        FORGOT_PASSWORD: false
    },
    PALETTE: ['#009dd9', '#d60037', '#b21dac', '#85c63f', '#ff8300', '#ffcd00', '#666666', '#cccccc'] // Nielsen
};