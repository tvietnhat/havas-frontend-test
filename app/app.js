(function () {
    'use strict';

    angular
        .module('app', [

            'env',
            'settings',
            'ui.router',
            'ui.bootstrap',
            'app.routes',
            'app.controllers',
			'angularResizable'
        ])
        .config(config)
        .run(run);

    function config($httpProvider, $compileProvider) {

        // https://code.angularjs.org/1.5.5/docs/guide/production
        $compileProvider.debugInfoEnabled(false);
    }

    function run($rootScope, $state, CONFIG, SETTINGS) {

        $rootScope.SETTINGS = SETTINGS;

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

            console.log('Entering state:', toState.name);
            var params = [];
            angular.forEach(toParams, function (p, key) {
                params.push(key + ' = ' + p);
            });

            if (params.length) {
                console.log('Params:        ', params.join(', '));
            }
        });
    }

})();