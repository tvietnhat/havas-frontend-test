(function () {
    'use strict';

    angular.module('settings', [])
        .constant('SETTINGS', (function () {

            var defaults = {
                DEFAULT_STATE: 'app.routes.default',
                DEFAULT_URL: '/',
                HTML5_MODE: true
            };

            // Overwrite defaults with customisations
            return angular.merge({}, defaults, SETTINGS);
        })())
    ;
})();
