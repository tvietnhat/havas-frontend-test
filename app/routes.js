(function () {
    'use strict';

    angular.module('app.routes', [])
        .config(Config)
        .run(Run)
    ;

    /* @ngInject */
    function Config($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider){

        $stateProvider
        .state('app',{
		    name: 'app.routes.default',
		    url: '/',
            templateUrl: 'views/index.html',
            controller:'HMTestCtrl',
            controllerAs:'vm'
        })
        ;

        // $urlRouterProvider.otherwise(SETTINGS.DEFAULT_URL);
        $locationProvider.html5Mode(true);

        // $httpProvider.interceptors.push('Loading');
    }

    /* @ngInject */
    function Run($rootScope, $location, CONFIG, SETTINGS){
        $rootScope.ENV = CONFIG.ENV;
        $rootScope.DEFAULT_URL = SETTINGS.DEFAULT_URL;
        $rootScope.goHome = goHome;

        function goHome() {
            $location.path(SETTINGS.DEFAULT_URL);
        }
    }

})();