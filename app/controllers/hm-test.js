(function() {
    'use strict';

    angular.module('app.controllers')

        .controller('HMTestCtrl', HMTestCtrl);

    /* @ngInject */
    function HMTestCtrl($scope) {

        var vm = this;

		vm.blocks = [ '', '', '', ''];
		
		vm.blockClick = function(blockIdx) {
			console.log(blockIdx);
			if (vm.blocks[blockIdx] === '') {
				for (var i = 0; i < vm.blocks.length; i++) {
					if (i === blockIdx) {
						vm.blocks[i] = 'active';
					} else {
						vm.blocks[i] = 'inactive';
					}
				}
			} else if (vm.blocks[blockIdx] === 'active') {
				for (var i = 0; i < vm.blocks.length; i++) {
					vm.blocks[i] = '';
				}
			}
		};
		
		vm.sidebarIsInactive = false;
		vm.toggleSidebar = function() {
			vm.sidebarIsInactive = !vm.sidebarIsInactive;
		};
    }

})();